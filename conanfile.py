from conans import CMake, ConanFile, tools


class XsimdConan(ConanFile):
    name = "xsimd"
    version = "7.2.1"
    license = "BSD3"
    url = "https://gitlab.com/mdavezac-conan/xsimd"
    homepage = "https://github.com/QuantStack/xsimd"
    description = "C++ wrappers for SIMD intrinsics"
    settings = "os", "arch", "compiler", "build_type"
    generators = "cmake"
    no_copy_source = True

    def source(self):
        git = tools.Git(folder="xsimd")
        git.clone(self.homepage, branch=self.version)

    def configured_cmake(self):
        cmake = CMake(self)
        cmake.configure(source_folder="xsimd")
        return cmake

    def build(self):
        return self.configured_cmake().build()

    def package(self):
        return self.configured_cmake().install()
